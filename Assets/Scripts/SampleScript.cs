using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleScript : MonoBehaviour
{
    private void Awake()
    {
        // 이 객체가 생성되었을 때 단 한번 호출되는 메서드입니다.
        Debug.Log("Awake Called!");
        // Debug 클래스 : 게임을 개발하는 동안 쉽게 디버깅 할 수 있는 메서드가 포함된 클래스입니다.
        // Log(stringData) : Unity 콘솔에 (stringData)메시지를 기록합니다.
    }

    private void Start()
    {
        // 컴포넌트가 활성화 된 경우 첫 번째 프레임 업데이트 전에 호출됩니다.
        Debug.Log("Start Called!");
    }

    private void FixedUpdate()
    {
        // Time -> FixedTimestep 에 설정된 시간마다 일정하게 호출됩니다.
    }

    private void Update()
    {
        // 매 프레임마다 호출되는 메서드입니다.
    }

    private void LateUpdate()
    {
        // Update 가 호출된 후 Frame 당 한 번 호출됩니다.
    }

    private void OnEnable()
    {
        // 컴포넌트가 활성화될 때 단 한번 호출됩니다.
        Debug.Log("OnEnable");
    }

    private void OnDisable()
    {
        // 컴포넌트가 비활성화될 때 단 한번 호출됩니다.
        Debug.Log("OnDisable");
    }

    private void OnDestroy()
    {
        // 제거될 경우 마지막으로 한번 호출됩니다.
        Debug.Log("OnDestroy");
    }
}
