using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /// <summary>
    /// 고양이 오브젝트
    /// </summary>
    public GameObject m_CatObject;

    // 최저 회전값
    public float m_MinRotation;
    // 최대 회전값
    public float m_MaxRotation;

    //적용시킬 회전값
    private float _CameraAngleX;

    private CatMovement _CatMovement;

    private void Start()
    {
        // m_CatObject 에 추가된 CatMovement 컴포넌트를 얻습니다.
        _CatMovement = m_CatObject.GetComponent<CatMovement>();
        // GetComponent<T>() : 반은 오브젝트에 추가된 T 형식의 컴포넌트를 얻습니다.

    }

    private void Update()
    {
        CalculateCameraAngleX();

        ApplyAngleX();
    }

    /// <summary>
    /// 카메라 회전값을 계산합니다.
    /// </summary>
    private void CalculateCameraAngleX()
    {
        // 고양이 위치
        Vector3 catPosition = m_CatObject.transform.position;

        // 고양이 위치를 축 값으로 변환합니다.
        float catPositionToAxis = (1 / _CatMovement.m_GroundRadius) * -1;

        // 회전값을 계산할 때 사용될 축 값
        float axis = catPosition.z * catPositionToAxis;

        // max ~ min 각도 사이의 차를 구합니다.
        float xAngleDist = m_MaxRotation - m_MinRotation;

        // 각도 차를 절반으로 나눕니다.
        // - (원을 절반으로 나눠 앞은 -1, 뒤는 1로 계산하기 위해
        float angleX = (xAngleDist * 0.5f) * axis;

        // 회전값을 완성합니다.   
        _CameraAngleX = (angleX + (xAngleDist * 0.5f) + m_MinRotation);
    }

    /// <summary>
    /// 계산된 회전값을 카메라에 적용합니다.
    /// </summary>
    private void ApplyAngleX()
    {
        // 선형보간 함수 (a, b, t)
        // 두 수가 주어졌을 때 그 사이에 위치한 값을 추정하기 위하여 직선 거리에 따라
        // 선형적으로 계산하는 방법
        // a : 시작 값
        // b : 끝 값
        // t : 얻고 싶은 값의 위치

        // 카메라 회전에 적용시키기 위한 Vector3 데이터
        Vector3 newEulerAngle = new();

        // 회전 속력
        float rotationSpeed = 3.0f;

        // 부드럽게 회전하도록 연산합니다.
        newEulerAngle.x = Mathf.Lerp(
            transform.eulerAngles.x,
            _CameraAngleX,
            rotationSpeed * Time.deltaTime);

        // 카메라 회전에 적용시킵니다.
        transform.eulerAngles = newEulerAngle;
    }

    public List<Vector3> results = new();
    Vector3 result = new Vector3(10, 10, 0);
    private void OnDrawGizmos()
    {
        // 시작 값
        Vector3 startPosition = new Vector3(10, 10, 0);

        // 끝 값
        Vector3 finishPosition = new Vector3(-10, 10, 0);

        result = startPosition;
        results.Clear();
        for(int i = 0; i< 20; ++i)
        {
            // 보간 결과를 잠시 보관해둘 변수
     
            // 선형보간 계산 확인
            result = Vector3.Lerp(result, finishPosition, 0.5f);

            // 구면보간
            //result = Vector3.Slerp(result, finishPosition, 0.5f);

            results.Add(result); // 리스트에 추가
        }

        Handles.color = Color.black;
        Gizmos.color = Color.blue;

        // 시작 위치 그리기
        Gizmos.DrawSphere(startPosition, 0.5f);
        Handles.Label(startPosition + (Vector3.up * 0.6f), "Start");

        // 끝 위치 그리기
        Gizmos.DrawSphere(finishPosition, 0.5f);
        Handles.Label(finishPosition + (Vector3.up * 0.6f), "Finish");

        Gizmos.color = Color.cyan;

        foreach(Vector3 res in results)
        {
            Gizmos.DrawSphere(res, 0.1f);
        }

    }

}
